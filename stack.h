/*
** stack.h for stl in stl
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-21 vermeille
** Last update 2012-11-10 12:15 sanche_g
*/

#ifndef STACK_H_
# define STACK_H_

# include <stdlib.h>

# include "list.h"

# define STACK(Type, Name)          \
    LIST(Type, Name)

# define STACK_CREATE(Stack)        \
    LIST_CREATE(Stack)

# define STACK_PUSH(Stack, El)      \
    LIST_INSERT_HEAD(Stack, El)

# define STACK_POP(Stack, OutEl)    \
    {                               \
        void *tmp = (Stack)->next;  \
                                    \
        OutEl = (Stack)->value;     \
        free(Stack);                \
        (Stack) = tmp;              \
    }

# define STACK_ISEMPTY(Stack)       \
    ((Stack) == NULL)

# define STACK_DESTROY(Stack)       \
    LIST_DESTROY((Stack))

# define STACK_TOP(Stack)           \
    ((Stack)->value)

#endif /* !STACK_H_ */

