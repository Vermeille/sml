/**
** @file map.h for stl in stl
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-19 vermeille
** Last update 2012-11-10 12:16 sanche_g
*/

#ifndef MAP_H_
# define MAP_H_

# include "list.h"

# define MAP_SIZE 127

/**
**@brief Declares an associative container
**
**@param KeyType
**@param ValueType
**@PARAM Name
*/
# define MAP(KeyType, ValueType, Name)                      \
    LIST(                                                   \
        struct                                              \
        {                                                   \
            KeyType   key;                                  \
            ValueType val;                                  \
        }, s_list_##Name)                                   \
    typedef s_list_##Name *Name;                            \
    typedef KeyType Name##_keytype;                         \
    typedef ValueType Name##_valuetype;                     \

/**
**@brief Instanciates a map
**
** @param Map instance
*/
# define MAP_CREATE(Map)                                    \
    (Map) = malloc(MAP_SIZE * sizeof (*(Map)[0]));          \
    for (size_t i = 0; i < MAP_SIZE; ++i)                   \
        LIST_CREATE((Map)[i]);                              \

/**
**@brief Inserts the pair Key Val in Map
**
**@param Map instance
**@param Key key
**@param Val value
**@param KeySize Number of elements in Key, 1 if it's not an array
*/
# define MAP_INSERT(Map, Key, Val, KeySize)                 \
    {                                                       \
        int hash = 0;                                       \
        void *tmp;                                          \
                                                            \
        for (size_t i = 0; i < KeySize; ++i)                \
            hash ^= Key[i];                                 \
        hash %= MAP_SIZE;                                   \
                                                            \
        tmp = (Map)[hash];                                  \
        (Map)[hash] = malloc(sizeof (*(Map)[0]));           \
        (Map)[hash]->value.val = Val;                       \
        (Map)[hash]->value.key = Key;                       \
        (Map)[hash]->next = tmp;                            \
    }

/**
**@brief Retrieves the value associated with Key in MAp
**
**@param Map instance
**@param Key key
**@param KeySize Number of elements in Key
**@param Comp a comparison function
**@param OutVal output parameter : associated value with Key, NULL is none is
**found
*/
# define MAP_GET(Map, Key, KeySize, Comp, OutVal)           \
    {                                                       \
        int hash = 0;                                       \
        void *tmp;                                          \
                                                            \
        for (size_t i = 0; i < KeySize; ++i)                \
            hash ^= Key[i];                                 \
                                                            \
        OutVal = NULL;                                      \
        tmp = (Map)[hash];                                  \
        while ((Map)[hash])                                 \
        {                                                   \
            if (Comp((Map)[hash]->value.key, Key))          \
            {                                               \
                OutVal = (Map)[hash]->value.val;            \
                break;                                      \
            }                                               \
            (Map)[hash] = (Map)[hash]->next;                \
        }                                                   \
        (Map)[hash] = tmp;                                  \
    }                                                       \

/**
**@brief Sets the value associated with Key in MAp
**
**@param Map instance
**@param Key key
**@param KeySize Number of elements in Key
**@param Comp a comparison function
**@param Val associated value with Key
*/
# define MAP_SET(Map, Key, KeySize, Comp, Val)              \
    {                                                       \
        int hash = 0;                                       \
        void *tmp;                                          \
                                                            \
        for (size_t i = 0; i < KeySize; ++i)                \
            hash ^= Key[i];                                 \
                                                            \
        tmp = (Map)[hash];                                  \
        while ((Map)[hash])                                 \
        {                                                   \
            if (Comp((Map)[hash]->value.key, Key))          \
            {                                               \
                (Map)[hash]->value.val = Val;               \
                break;                                      \
            }                                               \
            (Map)[hash] = (Map)[hash]->next;                \
        }                                                   \
        (Map)[hash] = tmp;                                  \
    }                                                       \

/**
** @brief Iter on every element of the map
**
** @param Map instance
** @param IterIdx name of iterator variable
** @param IterList name of iterator of list
** @param Body code to execute on every item
*/
# define MAP_FOREACH(Map, IterIdx, IterList, Body)          \
{                                                           \
    for (size_t IterIdx = 0; IterIdx < MAP_SIZE; ++IterIdx) \
        LIST_FOREACH(IterList, (Map)[IterIdx], Body);       \
}

/**
**@brief Destroys the map
**
**@param Map instance
*/
# define MAP_DESTROY(Map)                                   \
    for (size_t i = 0; i < MAP_SIZE; ++i)                   \
        LIST_DESTROY((Map)[i]);                             \
    free(Map);                                              \

#endif /* !MAP_H_ */

