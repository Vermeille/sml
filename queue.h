/*
** queue.h for stl in stl
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-22 vermeille
** Last update 2012-10-22 02:18 vermeille
*/

#ifndef QUEUE_H_
# define QUEUE_H_

# define QUEUE(Type, Name)                                     \
    LIST(Type, Name##_impl)                                    \
    typedef struct                                             \
    {                                                          \
        Name##_impl last;                                      \
        Name##_impl impl;                                      \
    } *Name;                                                   \

# define QUEUE_CREATE(Queue)                                   \
    Queue = malloc(sizeof (*Queue));                           \
    LIST_CREATE(Queue->impl);                                  \
    LIST_CREATE(Queue->last);                                  \
    Queue->last = NULL;

# define QUEUE_PUSH(Queue, El)                                 \
    {                                                          \
        void *tmp = Queue->impl;                               \
                                                               \
        if (LIST_ISEMPTY(Queue->impl))                         \
        {                                                      \
            Queue->impl = malloc(sizeof (Queue->last));        \
            Queue->impl->value = El;                           \
            Queue->impl->next = NULL;                          \
            Queue->last = Queue->impl;                         \
        }                                                      \
        else                                                   \
        {                                                      \
            Queue->last->next = malloc(sizeof (Queue->last));  \
            Queue->last = Queue->last->next;                   \
            Queue->last->value = El;                           \
            Queue->last->next = NULL;                          \
        }                                                      \
    }

# define QUEUE_POP(Queue, OutEl)                               \
    {                                                          \
        void *tmp = Queue->impl;                               \
                                                               \
        OutEl = Queue->impl->value;                            \
        Queue->impl = Queue->impl->next;                       \
        if (!Queue->impl)                                      \
            Queue->last = NULL;                                \
        free(tmp);                                             \
    }

# define QUEUE_ISEMPTY(Queue)                                  \
    (Queue->impl == NULL)


#endif /* !QUEUE_H_ */

