/*
** list.h for stl in stl
**
** Made by vermeille
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-19 vermeille
** Last update 2012-10-27 00:45 sanche_g
*/

#ifndef LIST_H_
# define LIST_H_

# include <stdlib.h>

# define LIST(Type, Name)                             \
    typedef struct list_##Name *Name;                 \
    struct list_##Name                                \
    {                                                 \
        Type value;                                   \
        Name next;                                    \
    };                                                \

# define LIST_CREATE(List)                            \
    List = NULL;

# define LIST_INSERT_HEAD(List, Val)                  \
    {                                                 \
        void *tmp = List;                             \
                                                      \
        List = malloc(sizeof (*List));                \
        List->value = Val;                            \
        List->next = tmp;                             \
    }                                                 \

# define LIST_FIND(List, Val, Out)                    \
    {                                                 \
        void *tmp = List;                             \
                                                      \
        Out = NULL;                                   \
        while (List)                                  \
        {                                             \
            if (List->value == Val)                   \
            {                                         \
                Out = List;                           \
                break;                                \
            }                                         \
            List = List->next;                        \
        }                                             \
        List = tmp;                                   \
    }                                                 \

# define LIST_FOREACH(Iter, List, Body)               \
    {                                                 \
        void *tmp = List;                             \
                                                      \
        while (List)                                  \
        {                                             \
            Iter = List;                              \
            Body                                      \
            List = List->next;                        \
        }                                             \
        List = tmp;                                   \
    }                                                 \

# define LIST_ISEMPTY(List)                           \
    (!List)

# define LIST_REMOVE(List, Delptr)                    \
    {                                                 \
        void *tmp;                                    \
        void *remember = List;                        \
                                                      \
        if (List == Delptr)                           \
        {                                             \
            tmp = List->next;                         \
            free(List);                               \
            List = tmp;                               \
        }                                             \
        else                                          \
        {                                             \
            while (List && List->next)                \
            {                                         \
                if (List->next == Delptr)             \
                {                                     \
                    tmp = List->next->next;           \
                    free(List->next);                 \
                    List->next = tmp;                 \
                    break;                            \
                }                                     \
                List = List->next;                    \
            }                                         \
            List = remember;                          \
        }                                             \
    }                                                 \

# define LIST_DESTROY(List)                           \
    {                                                 \
        void *tmp;                                    \
                                                      \
        while (List)                                  \
        {                                             \
            tmp = List->next;                         \
            free(List);                               \
            List = tmp;                               \
        }                                             \
    }

#endif /* !LIST_H_ */

