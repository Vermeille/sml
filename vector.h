/**
** @file vector.h for svmonitor in svmonitor
**
** Made by sanche_g
** Login   sanche_g guillaume.sanchez@epita.fr
**
** Started on  2012-10-18 sanche_g
** Last update 2012-10-28 06:00 sanche_g
*/

#ifndef VECTOR_H_
# define VECTOR_H_

# include <stdlib.h>

/**
**@brief Defines a kind of std::vector<Type>. It is defined with macros to be
**perfectly polymorphic. Compared to macros wich declares functions, it allows
**to have a perfectly unified interface, and still a typesafe datastructure
**
**@param Type The type to wrap in the std::vector
**@param Name Equivalent to `typedef std::vector<Type> Name`
*/
# define VECTOR(Type, Name)                                                  \
    typedef struct                                                           \
    {                                                                        \
        Type    *values;                                                     \
        size_t  size;                                                        \
        size_t  max_size;                                                    \
    } *Name;                                                                 \

/**
**@brief Vector constructor
**
**@param Vector Out parameter, vector's instance. Like
** `Vector = new std::vector<Type>()`
*/
# define VECTOR_CREATE(Vector)                                               \
        (Vector) = malloc(sizeof (*(Vector)));                               \
        (Vector)->values = realloc(NULL, sizeof (*(Vector)->values) * 8);    \
        (Vector)->size = 0;                                                  \
        (Vector)->max_size = 8;                                              \

/**
**@brief Adds an element to the vector (Vector)
**
**@param Vector vector's instance
**@param El The element to add
*/
# define VECTOR_ADD(Vector, El)                                              \
    {                                                                        \
        if ((Vector)->size == (Vector)->max_size)                            \
        {                                                                    \
            (Vector)->max_size *= 2;                                         \
            (Vector)->values = realloc((Vector)->values,                     \
                    sizeof (*(Vector)->values) * (Vector)->max_size);        \
        }                                                                    \
                                                                             \
        (Vector)->values[(Vector)->size] = El;                               \
        ++(Vector)->size;                                                    \
    }

/**
**@brief Deletes the nth element to (Vector)
**
**@param Vector instance
**@param N The index of element to delete
*/
# define VECTOR_DEL(Vector, N)                                               \
        (Vector)->values[N] = (Vector)->values[(Vector)->size - 1];          \
        --(Vector)->size;                                                    \
        if ((Vector)->size < (Vector)->max_size / 2)                         \
        {                                                                    \
            (Vector)->max_size /= 2;                                         \
            (Vector)->values = realloc((Vector)->values,                     \
                    sizeof (*(Vector)->values) * (Vector)->max_size);        \
        }                                                                    \

# define VECTOR_DEL_PRESERVE_ORDER(Vector, N)                                \
        for (size_t del_it = N; del_it < (Vector)->size - 1; ++del_it)       \
            (Vector)->values[del_it] = (Vector)->values[del_it + 1];         \
        --(Vector)->size;

/**
**@brief Foreach ($Iter in $(Vector)) do $Body. Note that you can access the
**index with magic variable i
**
**@param Iter Iteration's variable name
**@param Vector Instance
**@param Body Code to execute
*/
# define VECTOR_FOREACH(Iter, IterIdx, Vector, Body)                         \
        for (size_t IterIdx = 0; IterIdx < (Vector)->size; ++IterIdx)        \
        {                                                                    \
            Iter = &(Vector)->values[IterIdx];                               \
            { Body }                                                         \
        }                                                                    \

/**
**@brief Destructor
**
**@param Vector instance
*/
# define VECTOR_DESTROY(Vector)                                              \
        free((Vector)->values);                                              \
        free((Vector));                                                      \

/**
**@brief Returns vector's size
**
**@param Vector instance
*/
# define VECTOR_LENGTH(Vector)                                               \
        ((Vector)->size)

/**
**@brief is empty ?
**
**@param Vector instance
*/
# define VECTOR_ISEMPTY(Vector)                                              \
        ((Vector)->size == 0)

/**
**@brief gets nth's element
**
**@param Vector instance
**@param N Index
*/
# define VECTOR_NTH(Vector, N)                                               \
        ((Vector)->values[N])

#endif /* !VECTOR_H_ */

